 from delta import *
from pyspark.sql.functions import *
import pyspark
from pyspark.sql.functions import expr

builder = pyspark.sql.SparkSession.builder.appName("MyApp") \
     .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
     .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
spark = spark = configure_spark_with_delta_pip(builder).getOrCreate()

df_pop_stream = spark.readStream.format("delta").load("/tmp/pop")
df_stp_stream = spark.readStream.format("delta").load("/tmp/stp")
df_stp_stream.createOrReplaceTempView("df_stp_stream")
df_pop_stream.createOrReplaceTempView("df_pop_stream")

spark.sql("SELECT p.crn, guid, firstName, lastName, email, city, country, dob, income, date FROM df_pop_stream p join \
df_stp_stream d on p.crn = d.crn").writeStream.format("delta").option("checkpointLocation","/tmp/checkpoint").start("/tmp/join")