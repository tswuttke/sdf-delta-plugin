import mysql.connector
from delta import *
from pyspark.sql.functions import *
import pyspark
builder = pyspark.sql.SparkSession.builder.appName("MyApp").config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension").config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
spark = spark = configure_spark_with_delta_pip(builder).getOrCreate()
df_join = spark.readStream.format("delta").load("/tmp/join")
connection_props = {"host": 'localhost', "user": 'root', "password": 'Elizabeth3', "database": 'jarvis'}
sc = spark.sparkContext
brConnect = sc.broadcast(connection_props)

def process_row(row, curs_select, curs_insert, curs_update):
   pop_crn = row.__getitem__("crn")
   guid = row.__getitem__("guid")
   firstName = row.__getitem__("firstName")
   lastName = row.__getitem__("lastName")
   email = row.__getitem__("email")
   income = row.__getitem__("income")
   val = (pop_crn, guid, firstName, lastName, email, income)
   sql_string = """
       replace into delta(pop_crn,guid,firstName,lastName,email,income) values (%s,%s,%s,%s,%s,%s)
     """
   curs_update.execute(sql_string, val)


def process_partition(partition, id):
   connection_properties = brConnect.value
   database = connection_properties.get("database")
   user = connection_properties.get("user")
   pwd = connection_properties.get("password")
   host = connection_properties.get("host")
   db_conn = mysql.connector.connect(
     host=host,
     user=user,
     password=pwd,
     database=database
   )
   dbc_insert = db_conn.cursor(prepared=True)
   dbc_update = db_conn.cursor(prepared=True)
   dbc_select = db_conn.cursor(prepared=True)
   for row in partition.collect():
     process_row(row, dbc_select, dbc_insert, dbc_update)
   db_conn.commit()
   dbc_insert.close()
   dbc_update.close()
   dbc_select.close()

df_join.writeStream.foreachBatch(process_partition).trigger("15 seconds").start()