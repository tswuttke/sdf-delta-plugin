Delta Lake Spark Sink Plugin
=====================

Build
-----
To build this plugin:

```
   mvn clean package
```    

The build will create a .jar and .json file under the ``target`` directory.
These files can be used to deploy your plugins.

Deployment
----------
You can deploy your plugins using the CDAP CLI:

    > load artifact <target/sdf-delta-plugin-<version>.jar config-file <target/sdf-delta-plugin-<version>.json>
    

For some reason the jackson libs in the /lib directory conflict with this package. Update them with the following
- jackson-annotations-2.10.0.jar
- jackson-databind-2.10.0.jar
- jackson-module-scala_2.12-2.10.0.jar
- jackson-core-2.10.0.jar
