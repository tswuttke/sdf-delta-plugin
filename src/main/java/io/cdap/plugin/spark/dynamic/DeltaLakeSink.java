/*
 * ..
 */

package io.cdap.plugin.spark.dynamic;

import io.cdap.cdap.api.annotation.Description;
import io.cdap.cdap.api.annotation.Macro;
import io.cdap.cdap.api.annotation.Name;
import io.cdap.cdap.api.annotation.Plugin;
import io.cdap.cdap.api.data.format.StructuredRecord;
import io.cdap.cdap.api.data.schema.Schema;
import io.cdap.cdap.api.plugin.PluginConfig;
import io.cdap.cdap.api.spark.sql.DataFrames;
import io.cdap.cdap.etl.api.PipelineConfigurer;
import io.cdap.cdap.etl.api.StageConfigurer;
import io.cdap.cdap.etl.api.batch.SparkExecutionPluginContext;
import io.cdap.cdap.etl.api.batch.SparkPluginContext;
import io.cdap.cdap.etl.api.batch.SparkSink;

import io.cdap.plugin.common.LineageRecorder;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link SparkSink} that takes any scala code and executes it.
 */
@Plugin(type = SparkSink.PLUGIN_TYPE)
@Name("DeltaLakeSink")
@Description("Sink data to Delta Lake")
public class DeltaLakeSink extends SparkSink<StructuredRecord> {
  private static final Logger LOG = LoggerFactory.getLogger(DeltaLakeSink.class);

  private final transient Config config;

  public DeltaLakeSink(Config config) {
    this.config = config;
  }

  @Override
  public void configurePipeline(PipelineConfigurer pipelineConfigurer) throws IllegalArgumentException {
    // TODO
    StageConfigurer stageConfig = pipelineConfigurer.getStageConfigurer();
    stageConfig.setOutputSchema(stageConfig.getInputSchema());
  }

  @Override
  public void prepareRun(SparkPluginContext sparkPluginContext) throws Exception {
    Schema schema = sparkPluginContext.getInputSchema();
    if (schema != null && schema.getFields() != null) {
      recordLineage(sparkPluginContext, config.referenceName, schema, "Write", "Wrote to Scala Spark Sink.");
    }

  }

  @Override
  public void run(SparkExecutionPluginContext context, JavaRDD<StructuredRecord> javaRDD) throws Exception {
    List<StructField> fields  = new ArrayList<StructField>();
    for (Schema.Field field : context.getOutputSchema().getFields()) {
      Schema.Type type = field.getSchema().getType();
      if (type.equals(Schema.Type.STRING))
        fields.add(DataTypes.createStructField(field.getName(), DataTypes.StringType, true));
      else if (type.equals(Schema.Type.INT))
        fields.add(DataTypes.createStructField(field.getName(), DataTypes.IntegerType, true));
      else if (type.equals(Schema.Type.BYTES))
        fields.add(DataTypes.createStructField(field.getName(), DataTypes.ByteType, true));
      else if (type.equals(Schema.Type.DOUBLE))
        fields.add(DataTypes.createStructField(field.getName(), DataTypes.DoubleType, true));
      else if (type.equals(Schema.Type.LONG))
        fields.add(DataTypes.createStructField(field.getName(), DataTypes.LongType, true));
      else if (type.equals(Schema.Type.BOOLEAN))
        fields.add(DataTypes.createStructField(field.getName(), DataTypes.BooleanType, true));
      else
        fields.add(DataTypes.createStructField(field.getName(), DataTypes.StringType, true));
    }

    StructType schema = DataTypes.createStructType(fields);
    JavaRDD<Row> rowRDD = javaRDD.map(x -> DataFrames.toRow(x, schema));
    SQLContext sql = new SQLContext(context.getSparkContext());
    Dataset<Row> df = sql.createDataFrame(rowRDD, schema);
    df.write().format("delta").mode(SaveMode.Append).save(config.getPath());
  }

  /**
   * Configuration object for the plugin
   */
  public static final class Config extends PluginConfig {

    @Description("Destination path prefix. For example," +
      "'hdfs://mycluster.net:8200/output"
    )
    @Macro
    private final String path;

    @Name("referenceName")
    @Description("This will be used to uniquely identify this source/sink for lineage, annotating metadata, etc.")
    public String referenceName;

    public Config(String path, String referenceName) {
      this.path = path;
      this.referenceName = referenceName;
    }

    public String getPath() {
      return path;
    }

    public String getReferenceName() {
      return referenceName;
    }
  }

  private void recordLineage(SparkPluginContext context, String outputName, Schema tableSchema, String operationName,
                             String description) {
    LineageRecorder lineageRecorder = new LineageRecorder(context, outputName);
    lineageRecorder.createExternalDataset(tableSchema);
    List<String> fieldNames = tableSchema.getFields().stream().map(Schema.Field::getName).collect(Collectors.toList());
    if (!fieldNames.isEmpty()) {
      lineageRecorder.recordWrite(operationName, description, fieldNames);
    }
  }


}
