package io.cdap.plugin.spark.source;

import io.cdap.cdap.api.data.format.StructuredRecord;
import io.cdap.cdap.api.data.schema.Schema;
import io.delta.standalone.DeltaLog;
import io.delta.standalone.data.CloseableIterator;
import io.delta.standalone.data.RowRecord;
import org.apache.hadoop.conf.Configuration;

import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


public class StructuredRecordReceiver extends Receiver<StructuredRecord> {
    private static final Logger LOG = LoggerFactory.getLogger(StructuredRecordReceiver.class);

    private final boolean autoAcknowledge;
    private final BackoffConfig backoffConfig;
    private int previousFetchRate = -1;

    private transient AtomicInteger bucket;
    private transient ScheduledThreadPoolExecutor executor;
    private final Schema schema;
    CloseableIterator<RowRecord> iter;

    private static final String INTERRUPTED_EXCEPTION_MSG =
            "Interrupted Exception when sleeping during backoff.";


    public StructuredRecordReceiver(boolean autoAcknowledge, StorageLevel storageLevel, Schema schema) {
        this(autoAcknowledge, storageLevel, BackoffConfig.defaultInstance(), schema);
    }

    public StructuredRecordReceiver(boolean autoAcknowledge, StorageLevel storageLevel,
                          BackoffConfig backoffConfig, Schema schema) {
        super(storageLevel);
        this.schema = schema;
        this.autoAcknowledge = autoAcknowledge;
        this.backoffConfig = backoffConfig;
    }


    @Override
    public void onStart() {
        this.executor = new ScheduledThreadPoolExecutor(3, new LoggingRejectedExecutionHandler());
        this.executor.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
        this.executor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        this.executor.setRemoveOnCancelPolicy(true);
        this.bucket = new AtomicInteger();
        //Configure Executor Service
        // TODO - remove hard code
        DeltaLog log = DeltaLog.forTable(new Configuration(), "/tmp/out");

        iter = log.snapshot().open();

        //Schedule tasks to set the message rate and start the receiver worker.
        scheduleTasks();

        LOG.info("Receiver started execution");
    }

    @Override
    public void onStop() {
        //Shutdown thread pool executor
        if (executor != null && !executor.isShutdown()) {
            executor.shutdown();
            try {
                executor.awaitTermination(30, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                LOG.error("InterruptedException while waiting for executor to shutdown.");
            }
        }

        LOG.info("Receiver completed execution");
    }







    /**
     * Schedule tasks
     */
    public void scheduleTasks() {
        if (!this.isStopped()) {
            executor.scheduleAtFixedRate(this::updateMessageRateAndFillBucket, 0, 1, TimeUnit.SECONDS);
            executor.scheduleWithFixedDelay(this::receiveMessages, 100, 100, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * Fetch new messages for our subscription.
     * Implements exponential backoff strategy when a retryable exception is received.
     * This method stops the receiver if a non retryable ApiException is thrown by the Google Cloud subscriber client.
     */
    protected void receiveMessages() {
        int backoff = backoffConfig.getInitialBackoffMs();

        //Try with backoff until stopped or the task succeeds.
        while (!isStopped()) {
            fetchAndAck();
            return;
        }
    }

    /**
     * Fetch new messages, store in Spark's memory, and ack messages.
     * Based on SubscribeSyncExample.java in Google's PubSub examples.
     */
    protected void fetchAndAck() {
        //Get the maximun number of messages to get. If this number is less or equal than 0, do not fetch.
        int maxMessages = bucket.get();
        if (maxMessages <= 0) {
            return;
        }

        RowRecord row = null;
        int numRows = 0;
        List<StructuredRecord> messages = new ArrayList<>();

        // TODO - remove this hard code - the schema should be passed through from the DeltaLakeSource
        // context.getOutputSchema() - but for some reason it comes through as nULL
        Schema schema2 = Schema.recordOf("bahhh",
                Schema.Field.of("body", Schema.of(Schema.Type.STRING))
        );

        // Schema of Delta table is {long, long, string}
        while (iter.hasNext() & (numRows < maxMessages)) {
            row = iter.next();
            numRows++;
            // TODO - this field is hard coded - need to use the schema and iterate through it
            String c3 = row.getString("body");
            Map<String,String> data = new HashMap<>();
            data.put("body",c3);

            StructuredRecord.Builder builder = StructuredRecord.builder(schema2);

            messages.add(builder.set("body", c3).build());

        }


        //If there are no messages to process, continue.
        if (messages.isEmpty()) {
            return;
        }

        //Decrement number of available messages in bucket.
        bucket.updateAndGet(x -> x - messages.size());

        //Exit if the receiver is stopped before storing and acknowledging.
        if (isStopped()) {
            LOG.trace("Receiver stopped before store and ack.");
            return;
        }

        store(messages.iterator());


    }


    /**
     * Sleep for a given number of milliseconds, calculate new backoff time and return.
     *
     * This method stops the receiver is an InterruptedException is thrown.
     *
     * @param backoff the time in milliseconds to delay execution.
     * @return the new backoff delay in milliseconds
     */
    protected int sleepAndIncreaseBackoff(int backoff) {
        try {
            if (!isStopped()) {
                LOG.trace("Backoff - Sleeping for {} ms.", backoff);
                Thread.sleep(backoff);
            }
        } catch (InterruptedException e) {
            stop(INTERRUPTED_EXCEPTION_MSG, e);
        }

        return calculateUpdatedBackoff(backoff);
    }

    /**
     * Calculate the updated backoff period baded on the Backoff configuration parameters.
     *
     * @param backoff the previous backoff period
     * @return the updated backoff period for the next cycle.
     */
    protected int calculateUpdatedBackoff(int backoff) {
        return Math.min((int) (backoff * backoffConfig.getBackoffFactor()), backoffConfig.getMaximumBackoffMs());
    }

    /**
     * Get the rate at which this receiver should pull messages and set this rate in the bucket we use for rate control.
     * The default rate is Integer.MAX_VALUE if the receiver has not been able to calculate a rate.
     */
    protected void updateMessageRateAndFillBucket() {
        int messageRate = (int) Math.min(supervisor().getCurrentRateLimit(), Integer.MAX_VALUE);

        if (messageRate != previousFetchRate) {
            previousFetchRate = messageRate;
            LOG.trace("Receiver fetch rate is set to: {}", messageRate);
        }

        bucket.set(messageRate);
    }



    /**
     * Class used to configure exponential backoff for Pub/Sub API requests.
     */
    public static class BackoffConfig implements Serializable {
        final int initialBackoffMs;
        final int maximumBackoffMs;
        final double backoffFactor;

        static final BackoffConfig defaultInstance() {
            return new BackoffConfig(100, 10000, 2.0);
        }

        public BackoffConfig(int initialBackoffMs, int maximumBackoffMs, double backoffFactor) {
            this.initialBackoffMs = initialBackoffMs;
            this.maximumBackoffMs = maximumBackoffMs;
            this.backoffFactor = backoffFactor;
        }

        public int getInitialBackoffMs() {
            return initialBackoffMs;
        }

        public int getMaximumBackoffMs() {
            return maximumBackoffMs;
        }

        public double getBackoffFactor() {
            return backoffFactor;
        }
    }

    /**
     * Builder class for BackoffConfig
     */
    public static class BackoffConfigBuilder implements Serializable {
        public int initialBackoffMs = 100;
        public int maximumBackoffMs = 10000;
        public double backoffFactor = 2.0;

        protected BackoffConfigBuilder() {
        }

        public static BackoffConfigBuilder getInstance() {
            return new BackoffConfigBuilder();
        }

        public BackoffConfig build() {
            if (initialBackoffMs > maximumBackoffMs) {
                throw new IllegalArgumentException("Maximum backoff cannot be smaller than Initial backoff");
            }

            return new BackoffConfig(initialBackoffMs, maximumBackoffMs, backoffFactor);
        }

        public int getInitialBackoffMs() {
            return initialBackoffMs;
        }

        public BackoffConfigBuilder setInitialBackoffMs(int initialBackoffMs) {
            this.initialBackoffMs = initialBackoffMs;
            return this;
        }

        public int getMaximumBackoffMs() {
            return maximumBackoffMs;
        }

        public BackoffConfigBuilder setMaximumBackoffMs(int maximumBackoffMs) {
            this.maximumBackoffMs = maximumBackoffMs;
            return this;
        }

        public double getBackoffFactor() {
            return backoffFactor;
        }

        public BackoffConfigBuilder setBackoffFactor(int backoffFactor) {
            this.backoffFactor = backoffFactor;
            return this;
        }
    }

    /**
     * Rejected execution handler which logs a message when a task is rejected.
     */
    protected static class LoggingRejectedExecutionHandler implements RejectedExecutionHandler {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            LOG.error("Thread Pool rejected execution of a task.");
        }
    }
}
