package io.cdap.plugin.spark.source;

import io.cdap.cdap.api.data.format.StructuredRecord;
import io.cdap.cdap.api.data.schema.Schema;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.StreamingContext;
import org.apache.spark.streaming.dstream.ReceiverInputDStream;
import org.apache.spark.streaming.receiver.Receiver;

public class StructuredRecordInputDStream extends ReceiverInputDStream<StructuredRecord> {
    //private final PubSubSubscriberConfig config;
    private final StorageLevel storageLevel;
    private final boolean autoAcknowledge;
    private final Schema schema;

    /**
     * Constructor Method
     *
     * @param streamingContext Spark Streaming Context
     * @param storageLevel     Spark Storage Level for received messages
     * @param autoAcknowledge  Acknowledge messages
     */
    StructuredRecordInputDStream(StreamingContext streamingContext, Schema schema, StorageLevel storageLevel,
                                 boolean autoAcknowledge) {
        super(streamingContext, scala.reflect.ClassTag$.MODULE$.apply(StructuredRecord.class));
        //this.config = config;
        this.storageLevel = storageLevel;
        this.autoAcknowledge = autoAcknowledge;
        this.schema = schema;
    }


    @Override
    public Receiver<StructuredRecord> getReceiver() {
        return new StructuredRecordReceiver(
                this.autoAcknowledge,
                this.storageLevel,
                this.schema);
    }
}
