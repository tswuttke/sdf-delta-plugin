package io.cdap.plugin.spark.source;

import io.cdap.cdap.api.annotation.Description;
import io.cdap.cdap.api.annotation.Macro;
import io.cdap.cdap.api.annotation.Name;
import io.cdap.cdap.api.annotation.Plugin;
import io.cdap.cdap.api.data.format.StructuredRecord;

import io.cdap.cdap.api.data.schema.Schema;
import io.cdap.cdap.api.plugin.PluginConfig;
import io.cdap.cdap.etl.api.batch.SparkPluginContext;
import io.cdap.cdap.etl.api.streaming.StreamingContext;
import io.cdap.cdap.etl.api.streaming.StreamingSource;
import io.cdap.cdap.etl.api.streaming.StreamingSourceContext;
import io.cdap.cdap.format.io.StructuredRecordDatumWriter;
import io.cdap.plugin.common.LineageRecorder;
import io.cdap.plugin.spark.dynamic.DeltaLakeSink;

import io.delta.standalone.DeltaLog;
import io.delta.standalone.data.CloseableIterator;
import io.delta.standalone.data.RowRecord;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.dstream.DStream;
import org.apache.spark.streaming.dstream.ReceiverInputDStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.conf.Configuration;
import scala.collection.JavaConverters;
import scala.reflect.ClassTag;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Plugin(type = StreamingSource.PLUGIN_TYPE)
@Name("DeltaLakeSource")
@Description("Source data from Delta Lake")
public class DeltaLakeSource extends StreamingSource<StructuredRecord> {

    private final transient Config config;
    private static final Logger LOG = LoggerFactory.getLogger(DeltaLakeSource.class);
    public DeltaLakeSource(Config config) {
        this.config = config;

    }

    @Override
    public void prepareRun(StreamingSourceContext context) throws Exception {
        // TODO
    }

    @Override
    public JavaDStream<StructuredRecord> getStream(StreamingContext context) throws Exception {
        JavaStreamingContext sc = context.getSparkStreamingContext();
        ArrayList<DStream<StructuredRecord>> receivers = new ArrayList<>(1);

        ClassTag<StructuredRecord> tag = scala.reflect.ClassTag$.MODULE$.apply(StructuredRecord.class);
        for (int i = 1; i <= 1; i++) {
            // TOOD - for some reason context.getOutptSchema() is NULL! Also need to ideally populate the schema
            // from the delta file automatically rather than get the user to populate manually
            // TODO - need to pass the config.path() through to the InputDStream - atm the path is hardcoded to /tmp/out
            // TODO - remove the autoAcknowledge field from the StructuredRecordInputDStream constructor
            ReceiverInputDStream<StructuredRecord> receiverInputDStream =
                    new StructuredRecordInputDStream(sc.ssc(), context.getOutputSchema(), StorageLevel.MEMORY_ONLY(), true);
            receivers.add(receiverInputDStream);
        }

        DStream<StructuredRecord> dStream = sc.ssc()
                .union(JavaConverters.collectionAsScalaIterableConverter(receivers).asScala().toSeq(), tag);

        return new JavaDStream<>(dStream, tag);
    }

    public static final class Config extends PluginConfig {

        @Description("Destination path prefix. For example," +
                "'hdfs://mycluster.net:8200/output"
        )
        @Macro
        private final String path;

        @Name("referenceName")
        @Description("This will be used to uniquely identify this source/sink for lineage, annotating metadata, etc.")
        public String referenceName;

        public Config(String path, String referenceName) {
            this.path = path;
            this.referenceName = referenceName;
        }

        public String getPath() {
            return path;
        }

        public String getReferenceName() {
            return referenceName;
        }
    }

    private void recordLineage(SparkPluginContext context, String outputName, Schema tableSchema, String operationName,
                               String description) {
        LineageRecorder lineageRecorder = new LineageRecorder(context, outputName);
        lineageRecorder.createExternalDataset(tableSchema);
        List<String> fieldNames = tableSchema.getFields().stream().map(Schema.Field::getName).collect(Collectors.toList());
        if (!fieldNames.isEmpty()) {
            lineageRecorder.recordWrite(operationName, description, fieldNames);
        }
    }
}
